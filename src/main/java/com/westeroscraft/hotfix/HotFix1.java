package com.westeroscraft.hotfix;


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class HotFix1 extends JavaPlugin implements Listener{
	
	@Override
	public void onEnable() {
		this.getServer().getPluginManager().registerEvents(this, this);
	}
	
	@EventHandler
	public void stopfirepunchout(PlayerInteractEvent e) {
		if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block b = e.getClickedBlock().getRelative(e.getBlockFace());
			if(b != null && b.getType() == Material.FIRE) {
				if(!e.getPlayer().hasPermission("firepunch")) {
					e.getPlayer().sendBlockChange(b.getLocation(), Material.FIRE, b.getData());
					e.setCancelled(true);
				}
			}
		}
	}

}
